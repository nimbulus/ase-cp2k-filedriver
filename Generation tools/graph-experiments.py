"""Graph6

Read and write graphs in graph6 format.

Format
------

"graph6 and sparse6 are formats for storing undirected graphs in a
compact manner, using only printable ASCII characters. Files in these
formats have text type and contain one line per graph."

See http://cs.anu.edu.au/~bdm/data/formats.txt for details.

All connected graphs are known to be unique (ie no isomorphisms). Generating 
unique connected graphs is very hard, and this actually is the reason
they were collected from Brendan Mckay's collection of simple graphs at 
http://users.cecs.anu.edu.au/~bdm/data/graphs.html

"""
import networkx as nx
from pysmiles import write_smiles,read_smiles, fill_valence #https://github.com/pckroon/pysmiles
from ase.visualize import view
import ase.io
import utils
from os import listdir
from os.path import isfile, join
import itertools
import numpy as np
#import ase
#from pybel import *
import pybel
for num in [3,4,5,6]:
    mols = nx.read_graph6(str(num)+".g6")
    #mol.add_edges_from([(0, 1),(0,2),(0,3),(0,4),(1,4)])#nx.add_edges_from adds an edge between
    #nodes labeled by their number. 
    for mol in mols:
        write = False
        for idx in mol:
            mol.nodes[idx]['element']="C"
        for i in range(num):
            if len(mol[i])>4:
                pass #unreasonable to suspect 
            else:
                write = True
                
        if write:
            #print(write_smiles(mol))
            fill_valence(mol, respect_hcount=True)
            #print("===================\n after adding H")
            #print(write_smiles(mol))
            with open(str(num)+".smiles","a") as f:
                f.write(write_smiles(mol)+'\n')

#elimination of 3-cycles
#find_all_cycles
#playing with ASE to create input files for cheap optimization of 
#each "isomer" 
#the following uses code from the openbabel project - 
#*J. Cheminf.* (2019) **11**, Art. 49.<https://doi.org/10.1186/s13321-019-0372-5>. 
#https://open-babel.readthedocs.io/en/latest/3DStructureGen/SingleConformer.html#gen3d
#Smiles to 3d structures is not a trivial task, but has been extensively
#described before. 
#Consideration of saturation...
for num in [3,4,5,6]:
    with open(str(num)+".smiles",'r') as f:
        smiles = f.read()
        smiles = smiles.split('\n')
        for smile in smiles:
            if len(smile)>1:
                cycles = utils.find_all_cycles(read_smiles(smile))
                dothis = True #should I add this cycle (ie its not a len3 cycle?)
                for i in cycles:
                    if type(i) == type([]):
                        if len(i)==3:
                            dothis = False #skip all cycles of length 3
                #hopefully this will retr
                if dothis:
                    mymol = pybel.readstring("smi", smile)
                    print(smile)
                    mymol.make3D(forcefield="mmff94",steps=0)
                    #mymol.localopt() //best not to optimize immediately!
                    #mymol.draw()
                    mymol.write(format='xyz',filename=str(num)+"/"+smile+".xyz",overwrite=True)
                    #vmol = ase.io.read(filename=str(num)+"/"+smile+".xyz",format="xyz")
                    #view(vmol) #definitely dont view them all at once!

#pluck some hydrogens off...
for num in [3,4,5,6]:
    mypath = "/home/randon/Documents/homework/2020/mcgill/rustam/software/graph-generator/"+str(num)+"/"
    files = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    for file in files:
        xyz = ""
        with open(mypath+file,'r') as f:
            xyz = f.read()
        mymol = pybel.readstring("xyz",xyz)
        smi_rep = mymol.write(format="smi",opt={})[:-2] #get rid of the \t\n
        """graph_mol = read_smiles(smi_rep,explicit_hydrogen=True)
        #Remove every n hydrogens...
        hydrogen_nodes = []
        for i in list(graph_mol.nodes(data='element')): #[(nodeNum,element),...]
            if i[1]=='H':
                hydrogen_nodes.append(i[0])#the node in question...
        numToRemoveMax = int(np.floor(len(hydrogen_nodes)/2))""" #doing it from smiles string...
        #What if I can just remove the hydrogen 
        #straight from the xyz file???
            
        lines = xyz.split('\n')
        lines = lines[2:] #the first 2 lines are the num of atoms, and a blank line. 
        hydrogen_nodes = []
        for line in lines:
            if len(line)>0:
                if line[0]=='H':
                    hydrogen_nodes.append(lines.index(line))
        numToRemoveMax = int(np.floor(len(hydrogen_nodes))) #Removing half + 1 hydrogens 
        if numToRemoveMax>1:
            for i in range(numToRemoveMax-1):
                graphs = [] #create a list of graphs already made, and make sure to remove 
                #isomorphisms, as some rotations will be 
                #remove is *every* choice of n from m in list of len m
                remove = [g for g in itertools.combinations(hydrogen_nodes,i+1)]
                for choice in remove: #removing hydrogens
                    newMolecule = []# we're gonna construct this molecule line by line using the old xyz file
                    xyzfile = ""
                    for line in lines:
                        if lines.index(line) in choice:
                            #this is a hydrogen to get rid of...
                            pass
                        else:
                            newMolecule.append(line)
                    xyzfile = xyzfile+str(len(newMolecule)-1)+'\n'
                    for newline in newMolecule:
                        xyzfile = xyzfile+'\n'+newline
                    newMol = pybel.readstring("xyz",xyzfile)
                    newname = newMol.write()[:-2] #\t\n at the end of this string for silly reasons..
                    graph_representation = read_smiles(newname)#do not put explicit hydrogens here or it will think that every ring
                    #is a unique molecule, which is not true. If you need to check
                    #for explicit hydrogens ie to check that sp3-s moieties dont exist, 
                    #create a new smiles rep
                    graph_rep_explicit_H = read_smiles(newname,explicit_hydrogen=True)
                    append = True
                    for graph in graphs:
                        if nx.is_isomorphic(graph,graph_representation): #check for isomorphisms. 
                            #note: is_isomorphic is not solvable in polynomial time, but since
                            #our largest graphs have only 20 nodes, this is acceptable
                            append = False
                            break
                    #add logic here to prune any structures with SP3-S fragments?
                    
                    
                    for atom in newMol:
                        #scan through atoms, and check for their degree of undersaturatedness
                        if atom.atomicnum == 6:
                            degUnsat = atom.implicitvalence - atom.valence
                            if degUnsat>0:
                                #check that there is at least 1 hydrogen attached
                                #by valence - heavyvalence. 
                                numH = atom.valence - atom.heavyvalence
                                if numH==0:
                                    append = False #we don't want any "naked hydrogens" 
                                    #per our discussion on feb 15. 
                        #scan through the atoms, check their undersaturation degree. 
                    if append: #only if this is a new graph should we write it to disk
                        graphs.append(graph_representation)
                        with open(mypath+newname+".xyz",'w') as f:
                            f.write(xyzfile)
                    else:
                        pass #don't write this xyzfile to disk as it's a duplicate or its 
                        #fails a heuristic. 
        


        
                
  
