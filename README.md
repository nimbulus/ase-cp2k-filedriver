# ASE-CP2K filedriver

A small wrapper and calculator for CP2K for ASE. The calculator included in ASE relies on hardcoded expectations for outputs from CP2K_Shell - which is bad practice as the output from both CP2K_Shell and CP2K are intended for humans to view, and can change fluidly with one version to the next. This wrapper tries to avoid that by instead parsing Cp2K's stdout, and thus avoid the confusing and infuriating debug process, and instead allows one to manually view the output from Cp2k, and thus determine why it may or may not work.

This calculator only does force and energy calculations, as it assumes that the molecular dynamics, NEB, or geometry optimization will be done by ASE. 

## Using
`template.inp` contains the structure of a CP2K input file. It is a pseudo xml file, with elements that can be replaced by a string (or in the case of keywords kinds, and binary, a list of strings), when creating a calculator object. Further customization can be done by simply editing `template.inp` to reflect your usual calculations with CP2K, if additional keywords are desired. 

From the perspective of ASE, this behaves like any other calculator. It will return energy and forces, and will only calculate these anew if it detects the positions of the atoms, or the cell parameters, have changed.

## Quickstart

```python
#!/bin/python3
from CP2KD import CP2K, CP2KSECTIONS
...
calc = CP2K(atoms=image,
                kinds=["""
                    &KIND H
                        POTENTIAL GTH-PBE-q1
                        BASIS_SET DZVP-MOLOPT-SR-GTH-q1
                    &END KIND""", ...],
                #Each element of a list is the "KINDS" section for each element verbatim - see https://manual.cp2k.org/trunk/CP2K_INPUT/FORCE_EVAL/SUBSYS/KIND.html
                multiplicity="1",binary=['mpirun','-n','16','cp2k.popt'],
                charge="-1", moreoptions...
                          )
image.set_calculator(calc)

```
## Authors and acknowledgment
This driver relies on the cp2k-output-parser project to do some reading and interpretation of the output files made by CP2K. It also depends on CP2K itself. Finally, this driver is meant to be used with ASE.

## License
CP2K is available under the GPL; the GPLv3 is an appropriate license for this script as well :)

