from ase import io
import os
from ase.visualize import view
NIMAGES = 7 #the first and last image have no calculators attached as they are stationary
FILE = os.getcwd()+"/A2Bf.traj"
if os.path.isfile(FILE):
    traj = io.Trajectory(FILE, 'r')
    print("traj file found,",len(traj),"images in restart file")
    assert (len(traj) % NIMAGES) == 0
    steps = len(traj) // NIMAGES
    bands = []
    bande = []
    for step in range(steps):
        indices = range(step * NIMAGES, step * NIMAGES + NIMAGES)
        images = [traj[index] for index in indices]
        energies = [atoms.get_total_energy() for atoms in images]
        bands.append(images)
        bande.append(energies)
    maxE = max(bande[-1])
    guessTS = bands[-1][bande[-1].index(maxE)]
    io.write(filename="TS.xyz",images=guessTS)