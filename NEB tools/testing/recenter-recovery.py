from ase import io
import numpy as np
from ase.visualize import view
from ase import Atoms, Atom
#takes end and start
#uses start's GaN slab as reference point
#moves the end such that it's slab overlaps the start's slabs as much
#as possible
#Then re-orders the GaN slab of the end such that the ordering matches that of the start and each atom is close to the correct position


satoms = io.read("start.xyz")
satoms.pbc = (True,True,True)
satoms.cell = np.eye(3)*np.array([19.4735036,20.744,30.0])
satoms.center()

eatoms = io.read("end.xyz")
eatoms.pbc = (True,True,True)
eatoms.cell = np.eye(3)*np.array([19.4735036,20.744,30.0])
eatoms.center()

#Move the eatoms such that its lowest atom (thus presumably least impacted by reactions on the surface) is at the same level as the lowest atom in satoms
#Guessing that atom 0 for both are nitrogen
#This is in general, a "fixed" atom in the reaction. For any other reaction, it may be a good idea to specify which
#atom should remain fixed (and is in the same position in the start, end file)
def center(eatoms,satoms):
    assert eatoms[0].symbol==satoms[0].symbol=='N', "atom 0 of end, or of start, is not nitrogen, or they don't match!"
    dist = satoms[0].position - eatoms[0].position

#aligning the atoms
    for atom in eatoms:
        atom.position = atom.position+dist
center(eatoms,satoms)
#check that the order is correct
#This is an O(N^2) operation!! I don't know what would be a quicker way to check all the slab atoms are aligned correctly though :(

slabsym = ["Ga","N","C","H","U","I"]
#move all the nonslab atoms to the back of the list for satoms, eatoms
"""for atoms in [satoms,eatoms]:
    for atom in atoms:
        if atom.symbol not in slabsym:
            move = atoms.pop(atom.index)
            atoms.append(move)
            """
reorder = {"satoms":[],"eatoms":[]} 
#dict with satoms being the list of index of the closest atom to where the corresponding entry in eatoms is
#corrected for symbol

def findLikeliestAtom(idx,satoms,eatoms):
    myatom = eatoms[idx]
    closest = 1e3
    retsidx = -1
    for satom in satoms:
        if satom.symbol==myatom.symbol:
            dist = myatom.position - satom.position
            ddist = np.sqrt(dist.dot(dist))
            #print(ddist)
            if ddist<closest:
                closest = ddist
                retsidx = satom.index
    return retsidx
def dist(a,b):
    v = a.position - b.position
    return np.sqrt(v.dot(v))
def checkAndReOrder(reorder,eatoms,satoms):
    reorder = {"satoms":[],"eatoms":[]} 
    satomsTracker = satoms.copy()
    ambiguities = []
    eoccupied = []
    soccupied = []
    for eidx,eatom in enumerate(eatoms):
        #print(eidx)
        if eatom.symbol in slabsym:
            def getLikeliestAtomWithoutRepeats(prambiguous=False):
                sidx = findLikeliestAtom(eidx,eatoms,satomsTracker)
                if ((sidx in soccupied)):
                    print("found an ambigous atom for ",eidx,"-> ",sidx)
                    satomsTracker[sidx].symbol="Xe"
                    ambiguities.append(sidx)
                    #TODO: so it's already there! Check if the greedy approach, keeping old sidx the same, and issuing newsidx to 
                    #my eidx results in smaller dist than flipping it, 
                    #that is, turning old sidx into the new sidx, and assigning the old sidx to the current eidx
                    #choose the smaller option.
                    
                else:
                    if sidx in soccupied:
                        satomsTracker[sidx].symbol="Xe"
                        getLikeliestAtomWithoutRepeats()
                        ambiguities.append(sidx)
                        print("Unexpected sidx in soccupied....") #guaranteeing bijection
                        #idea: it's scanning through eidx 1 at a time, it will never repeat another eidx
                        #therefore, eoccupied is guaranteed to be unique
                        #to ensure soccupied is unique, if sidx in soccupied, choose another sidx.
                        #therefore we can gaurantee soccupied is unique
                        #and so a bijection exists between eoccupied and soccupied
                        #guaranteed 1:1 but havent gauranteed that this is the closest...
                    else:
                        #print(eidx,"->",sidx)
                        soccupied.append(sidx)
                        eoccupied.append(eidx)
                        if prambiguous:
                            ambiguities.append(sidx)
                    return sidx
            #there may be a possibility because of checking for ambiguities, if you do it an odd # of times, you will have
            #duplicate entries in soccupied. Check which one is supposed to be assigned to sidx = eidx, and which one is supposed
            #to be sidx = sidx (assert that eidx.symbol = sidx.symbol for both)
            #Alternative algorithm
            sidx = getLikeliestAtomWithoutRepeats()
    for i,entry in enumerate(soccupied):
        if entry!=eoccupied[i]:
            reorder['satoms'].append(entry)
            reorder['eatoms'].append(eoccupied[i])
    return reorder,soccupied,eoccupied, ambiguities



def unrepresented(reorder):
    satomstoEatoms = []
    eatomstoSatoms = []
    for i in reorder['satoms']:
        if i not in reorder['eatoms']:
            satomstoEatoms.append(i)
    for i in reorder['eatoms']:
        if i not in reorder['satoms']:
            eatomstoSatoms.append(i)
    return satomstoEatoms,eatomstoSatoms
def getOccurences(reorder):
    return [reorder['satoms'].count(l) for l in reorder['satoms']],[reorder['eatoms'].count(l) for l in reorder['eatoms']]
reorder, eocc,socc,amb = checkAndReOrder(reorder,eatoms,satoms)
assert unrepresented(reorder)==([],[])


#soccupied is the image, eocc is the image
#convert newE[eocc] = newE[socc], with the appropriate changing of the index tag. Assert that there are no 
#unrepresented objects (which should be taken care of by removing duplicates from soccupied)
def rearrange(socc,eocc):
    reorder = {"satoms":[],"eatoms":[]} 
    ceatoms = eatoms.copy()
    newE = [i for i in eatoms]
    for entry in reorder['satoms']:
        newE[entry]="PLACEHOLDER"
    for idx,atidx in enumerate(eocc): #socc is preimage of checkorder, eocc is image
        soccP = socc[idx]
        newE[soccP] = ceatoms[atidx] #invert the function
    r,socc,eocc,ambiguities = checkAndReOrder(reorder,Atoms(newE),satoms)
    return Atoms(newE),r,ambiguities
    
def fixed(r,a):
    if len(r['satoms'])>0:
        isCuzAmbigous = True
        for entry in r['satoms']:
            if entry in a:
                pass
            else:
                isCuzAmbigous = False
                print("failed to arrange correctly:",r,a)
                return False
        else:
            print("Ambiguous situation left:",r,a)
            return True
    else:
        return True

for i in range(3):
    eeatoms,r,a = rearrange(socc,eocc)
    print(i)
    if fixed(r,a):
        break