#!/usr/bin/python3

import os

files = os.listdir()
files.sort()
print(files)
for f in files:
    if "IMAGE" and "xyz" in f:
        with open(f,"r") as temp:
            guess = temp.read()
            guess = guess.split('\n')
            guess = guess[0]
print("Guessing "+guess+" atoms")
out = ""
NATOMS = input("number of atoms: ")
ENERG = ""
for f in files:
    if "IMAGE-" in f:
        print(f)
        with open(f,"r") as fl:
            contents = fl.read()
        contents = contents.split("\n"+NATOMS) #get the last iteration
        out = out +NATOMS+"\n" + contents[-1]
        #en = contents[-1].split('\n')[0].split(',')[1] #this hacky bit works for CP2K outputed xyzs
        #ENERG = ENERG+en+"\n"
with open("movie.xyz","w") as f:
    f.write(out)
with open("energies-images.txt","w") as f:
    f.write(ENERG)

with open("energies-images.txt","w") as ef:
    with open("energies.txt","r") as dat:
        a = dat.read()
        a = a.split("=============\n")
        interesting = a[-2]
        interesting = interesting.split('\n')[0]
        interesting = interesting.replace(']','')
        interesting = interesting.replace('[','')
        interesting = interesting.split(', ')
        for e in interesting:
            ef.write("E = "+e+"\n")
with open("qp.gp",'w') as f:
    f.write(""" 
set term png
set output "energies.png"
plot 'energies-images.txt' using 3 pointtype 7 pointsize 5
"""
)

import os
os.system("gnuplot qp.gp")
