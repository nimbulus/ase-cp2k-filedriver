#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 30 18:13:58 2022

@author: randon
"""
import numpy as np

tolerance = 0.1

with open('end.xyz','r') as f:
    ending = f.read()
with open('start.xyz','r') as f:
    start = f.read()


ending = ending.split('\n')
length = int(ending[0])
start = start.split('\n')
if length!=int(start[0]):
    print("different number of atoms!")
#assumes that the only errors here are transpositional (ie, Ga -> N). Does not attempt to check that the difference in position for any
#atom that is not suffering a transpositional error is in a reasonable position! Manual checking with visualization tools
#is still recommended!
def parsepos(xyzline):
    xyzline = xyzline.split('      ')
    if len(xyzline)==4:
        x = float(xyzline[1])
        y = float(xyzline[2])
        z = float(xyzline[3])
        return np.array([x,y,z])
def euclid(u1,u2):
    if type(u1)==type(np.array([1.0,1,1])):
        return np.sqrt( np.dot(u1-u2,u1-u2))
    else:
        return 0
eatoms = []
satoms = []
for line in ending[2:]:
    if len(line)>0:
        line = line.split('      ')
        atom = line[0]
        eatoms.append(atom)
for line in start[2:]:
    if len(line)>0:
        line = line.split('      ')
        atom = line[0]
        satoms.append(atom)

if len(eatoms)!=length:
    print("error with parsing xyz for ending file",len(eatoms),length)
    raise AssertionError
if len(satoms)!=length:
    print("error with parsing xyz for start file",len(satoms),length)
    raise AssertionError

for index,atom in enumerate(satoms):
    #print(index,"is")
    u1 = parsepos(ending[index+3])
    u2 = parsepos(start[index+3])
    dist = euclid(u1,u2)
    if dist>tolerance:
        print("line",index+4,"distance was:",dist)
    if satoms[index]!=eatoms[index]:
        print("mismatch at line", index+4, "got start:",satoms[index],"end was:",eatoms[index])

print("Done!")