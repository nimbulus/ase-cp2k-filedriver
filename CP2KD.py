#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  6 14:30:56 2021

@author: randon

todo:
    go through the warnings and return valueerror if SCF is not converged
    When generating multiple instances of the calculator, for some reason they're not independent instances... [done]
        any property declared with a class will be updated upon a new call to it. Any properties X created by "self.X" in __init__
        is unique to an instance of a class
    package cp2k_tools.cp2k_output_tools into something smaller 
"""
import numpy as np
import subprocess
from subprocess import CalledProcessError, PIPE
from ase.units import Rydberg
import os
import random
from cp2k_tools.cp2k_output_tools import parse_iter
from ase.calculators.calculator import (Calculator, all_changes, Parameters,
                                        CalculatorSetupError, FileIOCalculator)

class CP2K(FileIOCalculator):
    """My own ASE-Calculator for CP2K that does not rely on the strange
    and mysterious cp2k_shell, and instead runs a CP2K job in the background 
    and interprets the output files emitted by CP2K.
    At this time, I'm only going to let it emit energy, since everything else 
    be calculated from that by ASE. 
    
    The reason I made this is because the Cp2K_shell based calculator is very fragile and depends on hardcoded
    expectations of what the shell will emit. That is not guaranteed to be stable, and *will* change depending
    on how its compiled. OTOH, the output files should be relatively stable in the sense that energy will be labeled
    
    Warning: As it stands, this will not intelligently restart. It will write restart files to a given 
    directory, which you should check in your code if a restart file exists. If one does, you can then pass that as an 
    input file to this "calculator" to continue a restart. 
    
    That said energy should be fast enough to calculate within the 72 hours limit on computecanada anyways...
    
    You must supply your own kind
    
    CP2K is freely available under the GPL license.
    
    call by CP2K(atoms=Atoms,)
    """
    implemented_properties = ['energy','forces','stress']
    
    _wkdir = ""
    _natoms = 0
    _oldatoms = None
    _id = 0
    params = ['{{PRINT_LEVEL}}',
 '{{PROJECT_NAME}}',
 '{{CELL_PERIODIC}}',
 '{{ABC}}',
 '{{COORD_FILE_NAME}}',
 '{{POTENTIAL_FILE_NAME}}',
 '{{SURFACE_DIPOLE_CORRECTION}}',
 '{{SURF_DIP_DIR_ENABLE_AND_DIRECTION}}',
 '{{SURF_DIP_SWITCH}}',
 '{{UKS}}',
 '{{CHARGE}}',
 '{{MULTIPLICITY}}',
 '{{BASIS_SET_FILE_NAME}}',
 '{{NGRIDS}}',
 '{{CUTOFF}}',
 '{{EPS_DEFAULT}}',
 '{{REFERENCE_FUNCTIONAL}}',
 '{{PARAMETER_FILE_NAME}}',
 '{{KPOINTS_SECTION}}',
 '{{POISSONBLOCK}}',
 '{{MAX_SCF}}',
 '{{EPS_SCF}}',
 '{{OUTER_SCF}}',
 '{{OUTER_SCF_MAX_SCF}}',
 '{{SCF_OPTIMIZER}}',
 '{{OT_PRECONDITIONER}}',
 '{{OT}}',
 '{{KINDSECTION}}',
 '{{CALC_ID}}',
 '{{SMEARSECTION}}']
    cp2ktemplate = ""
    results = {}
    CP2Kworker = None
    def _generateTopology(self,atoms):
        self.results = {'energy':0.0,'forces':np.zeros(shape=(len(atoms), 3))}
        self._natoms = len(atoms)
        self.atoms = atoms
        pbc = atoms.pbc
        self._default_parameters['cell_periodic'] = ""
        self._default_parameters['abc'] = ""
        for i,u in enumerate(['X',"Y",'Z']):
            if pbc[i]:
                self._default_parameters['cell_periodic'] +=u
            self._default_parameters['abc'] += str(sum(atoms.cell[i]))+" "

    def __init__(self, restart=None,
                 ignore_bad_restart_file=Calculator._deprecated,
                 label='CP2KD', atoms=None, command=None,binary=["cp2k"],
                 debug=False, **kwargs):
        
        self._default_parameters = dict(
        auto_write=False,
        basis_set='DZVP-MOLOPT-SR-GTH',
        basis_set_file_name='BASIS_MOLOPT',
        cutoff="300",
        inp='',
        potential_file_name='GTH_POTENTIALS',
        parameter_file_name='dftd3.dat',
        uks=".TRUE.",
        poisson_solver='PERIODIC XYZ',
        cell_periodic="",
        abc= "",
        xc='PBE',
        project_name = "cp2k",
        coord_file_name = "cp2k.xyz",
        surf_dip_dir_enable_and_direction="", #SURF_DIP_DIR Z for slabs
        surface_dipole_correction = ".FALSE.", #.TRUE. for slabs
        surf_dip_switch = '',
        charge = "0", #determined by atoms object
        multiplicity = "1",
        ngrids = "4",
        eps_default = "1e-12",
        reference_functional = "PBE",
        kpoints_section = "",
        poissonblock = """
&POISSON
  PERIODIC XYZ
&END POISSON
    """,
        max_scf = "100",
        eps_scf = "1e-7",
        outer_scf = ".TRUE.",
        outer_scf_max_scf = "50",
        smearsection = "",
        scf_optimizer = "NONE",
        ot_preconditioner = "FULL_KINETIC",
        ot = ".TRUE.",
        kindsection = "",
        kinds=[],
        calc_id="0",
        print_level='MEDIUM',
        wkdir = os.getcwd()+"/cp2k_tmp"+str(self._id)
        ) #end dict
        
        """Construct CP2K-calculator object."""
        self._debug = debug
        self.label = "CP2KD"
        self.parameters = None        
        self.binary = binary
        self._id = str(random.randint(1,1000))
        if 'wkdir' in kwargs.keys():
            self._wkdir = kwargs['wkdir']
        else:
            self._wkdir = self._default_parameters['wkdir']
        try:
            os.mkdir(self._wkdir)
        except FileExistsError as fe:
            print(fe)
            print("cp2k_tmp",self._id,"exists, making newd")
            self._id = str(random.randint(1,1000))
            self._wkdir = os.getcwd()+"/cp2k_tmp"+self._id
            os.mkdir(self._wkdir)
        print("scratch directory:",self._wkdir)
        
        if restart is not None:
            with open(restart,'r') as f:
                self._default_parameters['inp'] = f.read()
            return #dont do any further processing since everything is written...
        # Several places are check to determine self.command
        if command is not None:
            self.command = command
        else:
            self.command = self.binary+[self._wkdir+"/cp2k.inp"]  # default
        #sanity check for mandatory needed stuff
        if "cp2kinp" not in kwargs.keys():
            with open("template.inp",'r') as f:
                self.cp2ktemplate = f.read()
        
        if "kinds" not in kwargs.keys():
            raise ValueError("Must supply a list of strings representing KIND for each element!","use the kinds= keywords")
        else:
            self._default_parameters['kindsection'] = ""
            for kind in kwargs['kinds']:
                self._default_parameters['kindsection'] += kind        
        
        #filling in kwargs
        for key in kwargs:
            if key=="cp2kinp":
                self.cp2ktemplate = kwargs[key]
            if key=="kinds":
                self._default_parameters['kinds'] = kwargs[key]
            if key in self._default_parameters.keys():
                self._default_parameters[key] = kwargs[key]
        self._default_parameters['coord_file_name'] = self._wkdir+"/cp2k.xyz"
        self._default_parameters['calc_id'] = str(self._id)
        self._generateTopology(atoms)
        super().__init__(label=label, atoms=atoms, **kwargs)

    def read(self,outputfile,**kwargs):
        #print(outputfile,kwargs)
        'Read atoms, parameters and calculated results from restart files.'
        #dummy stuff rn
        resultslist = []
        with open(outputfile,'r') as f:
            for match in parse_iter(f.read()):
                resultslist.append(match)
        for result in resultslist:
            if 'energies' in result.keys():
                self.results.update({'energy':result['energies']['total force_eval']})
            if 'forces' in result.keys():
                forcearray = np.zeros(shape=(self._natoms,3 ))
                for i,atom in enumerate(result['forces']['atomic']['per_atom']):
                    forcearray[i][0] = atom['x']
                    forcearray[i][1] = atom['y']
                    forcearray[i][2] = atom['z']
                self.results.update({'forces':forcearray})
    def write(self,inputfile):
        with open(self._wkdir+"/cp2k.inp",'w') as f:
            f.write(self.cp2ktemplate)

    def set(self, **kwargs):
        """Set parameters like set(key1=value1, key2=value2, ...)."""
        msg = '"%s" is not a known keyword for the CP2K calculator. ' \
              'To access all features of CP2K by means of an input ' \
              'template, consider using the "inp" keyword instead.'
        #self._generateTopology(super())
        for key in kwargs:
            if key not in self._default_parameters:
                raise CalculatorSetupError(msg % key)
        changed_parameters = Calculator.set(self, **kwargs)
        if changed_parameters:
            self.reset()
    def prepInputFile(self,text):
        for p in self.params:
            key = p.lower().replace("{{","")
            key = key.replace("}}","")
            text = text.replace(p,self._default_parameters[key])
        return text
    def calculate(self, atoms=None, properties=None,
                  system_changes=all_changes):
        """Do the calculation."""
        if not properties:
            properties = ['energy']
        #Calculator.calculate(self, atoms, properties, system_changes)
        print("calculating",str(properties),"on geometry")
        
        def spawnCP2K():
            print("launching CP2K worker using",self._wkdir+"/cp2k.inp","for input and",self._wkdir+"/cp2k.out-001"," for output")
            #print(self.command)
            with open(self._wkdir+"/cp2k.out-001",'w') as of:
                with open(self._wkdir+"/cp2k.stde",'w') as ef:
                    self.CP2Kworker = subprocess.run(self.command,stdout=of,stderr=ef) #this may be a bad idea to leave it as PIPE
                        #if stdout is too big, it *will* hang or crash!
            if self.CP2Kworker.returncode != 0:
                print("\n\n! An error has occured - inspect",self._wkdir+"/cp2k.stde and /cp2k.out-001","for further details!\n\n")
                raise ValueError("worker failed to complete job; errocode:",self.CP2Kworker.returncode,"You can try executing cp2k on cp2k.inp and seeing the error message")
                with open(self._wkdir+"cp2k.out-001","w") as of:
                    of.write(self.CP2Kworker.stdout)
            else:
                pass
                #print("done!")
        
        p = self.prepInputFile(self.cp2ktemplate)
        atoms.write(self._wkdir+"/cp2k.xyz")
        with open(self._wkdir+"/cp2k.inp",'w') as inf:
            inf.write(p)
        #check if system has changed since last calculation!
        def sysChanged():
            #print(self._oldatoms,atoms)
            if self._oldatoms == atoms:
                return False
            else:
                return True
        if sysChanged():
            spawnCP2K()
            self._oldatoms = atoms.copy()
            self.read(self._wkdir+"/cp2k.out-001")
        else:
            self.read(self._wkdir+"/cp2k.out-001")
class CP2KSECTIONS():
    def __init__(self):
        pass
    PBE_kinds = {'C':"""
    &KIND C
      POTENTIAL GTH-PBE-q4
      BASIS_SET DZVP-MOLOPT-SR-GTH-q4
    &END KIND
    """,
    'N':"""
    &KIND N
      POTENTIAL GTH-PBE-q5
      BASIS_SET DZVP-MOLOPT-SR-GTH-q5
    &END KIND
    """,
    'H':"""
    &KIND H
      POTENTIAL GTH-PBE-q1
      BASIS_SET DZVP-MOLOPT-SR-GTH-q1
    &END KIND
    """,
    'Ga':"""
    &KIND Ga
      POTENTIAL GTH-PBE-q13
      BASIS_SET DZVP-MOLOPT-SR-GTH-q13
    &END KIND
    """,
    'Cl':"""
    &KIND Cl
        ELEMENT Cl
        POTENTIAL GTH-PBE-q7
        BASIS_SET DZVP-MOLOPT-SR-GTH-q7
    &END KIND
    """,
    'GaPlus':"""
    &KIND GaPlus
      ELEMENT Ga
      POTENTIAL GTH-PBE-q13
      BASIS_SET DZVP-MOLOPT-SR-GTH-q13
      &BS
         &ALPHA 
            NEL     -1
            L        0
            N        4
         &END ALPHA
      &END BS
    &END KIND
    """,# a special one for my uses
    'NMinus':"""
     &KIND NMinus
      ELEMENT N
      POTENTIAL GTH-PBE-q5
      BASIS_SET DZVP-MOLOPT-SR-GTH-q5
      &BS
         &ALPHA
            NEL     +1
            L        1
            N        2
         &END ALPHA
      &END BS
    &END KIND
    """,
    }
    KPOINTS = """    
    KPOINTS
      FULL_GRID .TRUE.
      SCHEME {{SCHEME}} {{KPTS}} # default scheme is MONKHORST-PACK
    &END KPOINTS
    """
    
    def generateKpts(self,scheme,kpts):
        self.KPOINTS = self.KPOINTS.replace("""{{SCHEME}}""",scheme)
        self.KPOINTS = self.KPOINTS.replace("""{{KPTS}}""",kpts)
    
